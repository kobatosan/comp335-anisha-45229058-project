// https://stackoverflow.com/questions/20631666/java-socket-programming-conversation




import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private PrintStream print;

    /**
     * @param args
     * @throws Exception
     */

    public static void main(String[] args)
            throws  Exception { // to catch for errors earlier in the program and to be handled by JVM
        Server server = new Server(); // declaring
        server.start();
    }

    /**
     * @throws Exception
     */

    public void start() throws Exception {
      InetAddress address = InetAddress.getByName("127.0.0.1");  // establish connection to Client IP; https://docs.oracle.com/javase/8/docs/api/java/net/InetAddress.html#getByName-java.lan.String-
        ServerSocket serverSocket = new ServerSocket(8096, 1, address); // expect connection to one server, hence '1'
        System.out.println("server started, waiting for connection");

        Socket socket = serverSocket.accept();

        InputStreamReader iReader = new InputStreamReader(socket.getInputStream());
        BufferedReader bReader = new BufferedReader(iReader); // puts server message into buffer
        print = new PrintStream(socket.getOutputStream());

        while(true) {
            String clientMessage = bReader.readLine();
            System.out.println(clientMessage);

            if(clientMessage.trim().equalsIgnoreCase("hi")) {
                print.println("enter something"); // send message to client
            }

               else if(clientMessage.trim().equalsIgnoreCase("bye")) {
                  print.println("Closing connection");
                    break; // exit loop
                }

        } // end while



    } // close start method

}
