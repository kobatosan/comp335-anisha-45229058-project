import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

// invalid statements freezes simulator



public class Client {

    /**
     * @param args
     * @throws Exception
     */

    public static void main(String[] args)
        throws  Exception { // to catch for errors earlier in the program and to be handled by JVM
        Client client = new Client(); // declaring
        client.start();
    }

    /**
     * @throws Exception
     */

    public void start() throws Exception {
        BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in)); // enables message from server
        Socket socket = new Socket("127.0.0.1", 8096);
        PrintStream print = new PrintStream(socket.getOutputStream()); // enable message to server
        print.println("hi");

        InputStreamReader iReader = new InputStreamReader(socket.getInputStream());
        BufferedReader bMessage = new BufferedReader(iReader); // puts server message into buffer
        String serverMessage = bMessage.readLine().trim(); // server message stored here; trim used to cut down extra spacings of server message
        System.out.println(serverMessage);

        while(true) {
            serverMessage = bMessage.readLine();
            System.out.println(serverMessage);

            if(serverMessage.equalsIgnoreCase("enter something")) {
                String clientMessage = bReader.readLine().trim();
                print.println(clientMessage); // send message to server

                if(clientMessage.equalsIgnoreCase("bye")) {
                    System.out.println("Closing connection");
                    break; // exit loop
                }

                serverMessage = bMessage.readLine().trim();
                System.out.println(serverMessage);
            } // end first if
        } // end while



    } // close start method



}
